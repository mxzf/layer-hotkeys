# Layer Hotkeys

This module adds hotkey bindings for activating various canvas layers.

The default keybindings are Shift+1-8 and they can be rebound using the standard Configure Controls interface.

This should theoretically also support any modules/systems that add layers or tools through the normal framework.  Due to the order of operations when Foundry is initializing the page, there's a button in the settings which will save the layers and tools at the time it's clicked so that the information can be used starting on the next page refresh. 

## Installation

Module manifest: https://gitlab.com/mxzf/layer-hotkeys/-/releases/permalink/latest/downloads/module.json


