# Changelog

## 2.5.0

* Added optional support for binding some common combat tracker actions (enabled via a setting)

## 2.4.1

* Fixed a bug that caused toggles not to render their new state properly

## 2.4.0

* Added support for binding hotkeys for the right sidebar tabs (with a checkbox in the settings to enable them)

## 2.3.0

This theoretically adds support for any modules/systems that add layers or tools through the normal framework.  Due to the order of operations when Foundry is initializing the page, I've added a button in the settings which will save the layers and tools at the time it's clicked so that the information can be used starting on the next page refresh. 

### Features

* Added buttons to the settings for saving/resetting layers+tools
* Added iteration functionality to add hotkeys for arbitrary layers+tools

## 2.2.0

### Semi-breaking change

Due to a change in the way hotkeys are registered (to enable handling all of the individual tools inside the layers), previous keybindings will not be recognized and will need to be assigned again.  Unless core Foundry renames the layers, this should be a one-time thing.  

### Features

* Added settings to enable adding keybindings for the various tools within the layers
* Added setting for resetting to the first tool in a layer whenever you switch layers

## 2.1.0

Added setting to enable cycling between tools within a layer by repeatedly pressing the layer's hotkey

## 2.0.0 - Version bump

Version bump to get the version numbering in-line with the old 0.7.x-era module

## 0.1.0 - Initial relase

Initial release of the module

### Features 

* Hotkey bindings for the eight core canvas layers

## 