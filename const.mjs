const {SHIFT, CONTROL, ALT} = KeyboardManager.MODIFIER_KEYS;
export const DEFAULT_BINDINGS = {
    'tokens': [{key: "Digit1", modifiers: [SHIFT]}],
    'templates': [{key: "Digit2", modifiers: [SHIFT]}],
    'tiles': [{key: "Digit3", modifiers: [SHIFT]}],
    'drawings': [{key: "Digit4", modifiers: [SHIFT]}],
    'walls': [{key: "Digit5", modifiers: [SHIFT]}],
    'lighting': [{key: "Digit6", modifiers: [SHIFT]}],
    'sounds': [{key: "Digit7", modifiers: [SHIFT]}],
    'notes': [{key: "Digit8", modifiers: [SHIFT]}],
}

// ui.controls.controls.map(({layer,title,tools}) => ({layer,title,tools:tools.filter(t=> !(t?.button || t?.toggle)).map(({name,title}) => ({name,title}))}))
// ui.controls.controls.map(({layer,title,tools}) => ({layer,title,tools:tools.map(({name,title,button,toggle}) => ({name,title,button,toggle}))}))
export const CORE_LAYERS = [
    {"layer": "tokens","title": "CONTROLS.GroupToken",
        "tools": [
            {"name": "select","title": "CONTROLS.BasicSelect"},
            {"name": "target","title": "CONTROLS.TargetSelect"},
            {"name": "ruler","title": "CONTROLS.BasicMeasure"}
        ]
    },
    {"layer": "templates","title": "CONTROLS.GroupMeasure",
        "tools": [
            {"name": "circle","title": "CONTROLS.MeasureCircle"},
            {"name": "cone","title": "CONTROLS.MeasureCone"},
            {"name": "rect","title": "CONTROLS.MeasureRect"},
            {"name": "ray","title": "CONTROLS.MeasureRay"},
            {"name": "clear","title": "CONTROLS.MeasureClear","button": true}
        ]
    },
    {"layer": "tiles","title": "CONTROLS.GroupTile",
        "tools": [
            {"name": "select","title": "CONTROLS.TileSelect"},
            {"name": "tile","title": "CONTROLS.TilePlace"},
            {"name": "browse","title": "CONTROLS.TileBrowser","button": true},
            {"name": "foreground","title": "CONTROLS.TileForeground","toggle": true}
        ]
    },
    {"layer": "drawings","title": "CONTROLS.GroupDrawing",
        "tools": [
            {"name": "select","title": "CONTROLS.DrawingSelect"},
            {"name": "rect","title": "CONTROLS.DrawingRect"},
            {"name": "ellipse","title": "CONTROLS.DrawingEllipse"},
            {"name": "polygon","title": "CONTROLS.DrawingPoly"},
            {"name": "freehand","title": "CONTROLS.DrawingFree"},
            {"name": "text","title": "CONTROLS.DrawingText"},
            {"name": "configure","title": "CONTROLS.DrawingConfig","button": true},
            {"name": "clear","title": "CONTROLS.DrawingClear","button": true}
        ]
    },
    {"layer": "walls","title": "CONTROLS.GroupWall",
        "tools": [
            {"name": "select","title": "CONTROLS.WallSelect"},
            {"name": "walls","title": "CONTROLS.WallDraw"},
            {"name": "terrain","title": "CONTROLS.WallTerrain"},
            {"name": "invisible","title": "CONTROLS.WallInvisible"},
            {"name": "ethereal","title": "CONTROLS.WallEthereal"},
            {"name": "doors","title": "CONTROLS.WallDoors"},
            {"name": "secret","title": "CONTROLS.WallSecret"},
            {"name": "clone","title": "CONTROLS.WallClone"},
            {"name": "snap","title": "CONTROLS.WallSnap","toggle": true},
            {"name": "clear","title": "CONTROLS.WallClear","button": true}
        ]
    },
    {"layer": "lighting","title": "CONTROLS.GroupLighting",
        "tools": [
            {"name": "light","title": "CONTROLS.LightDraw"},
            {"name": "day","title": "CONTROLS.LightDay","button": true},
            {"name": "night","title": "CONTROLS.LightNight","button": true},
            {"name": "reset","title": "CONTROLS.LightReset","button": true},
            {"name": "clear","title": "CONTROLS.LightClear","button": true}
        ]
    },
    {"layer": "sounds","title": "CONTROLS.GroupSound",
        "tools": [
            {"name": "sound","title": "CONTROLS.SoundDraw"},
            {"name": "preview","title": "CONTROLS.SoundPreview","toggle": true},
            {"name": "clear","title": "CONTROLS.SoundClear","button": true}
        ]
    },
    {"layer": "notes","title": "CONTROLS.GroupNotes",
        "tools": [
            {"name": "select","title": "CONTROLS.NoteSelect"},
            {"name": "journal","title": "NOTE.Create"},
            {"name": "toggle","title": "CONTROLS.NoteToggle","toggle": true},
            {"name": "clear","title": "CONTROLS.NoteClear","button": true}
        ]
    }
]

export const TABS = {
    chat: {
        tooltip: ChatMessage.metadata.labelPlural,
        icon: CONFIG.ChatMessage.sidebarIcon
    },
    combat: {
        tooltip: Combat.metadata.labelPlural,
        icon: CONFIG.Combat.sidebarIcon
    },
    scenes: {
        tooltip: Scene.metadata.labelPlural,
        icon: CONFIG.Scene.sidebarIcon
    },
    actors: {
        tooltip: Actor.metadata.labelPlural,
        icon: CONFIG.Actor.sidebarIcon
    },
    items: {
        tooltip: Item.metadata.labelPlural,
        icon: CONFIG.Item.sidebarIcon
    },
    journal: {
        tooltip: JournalEntry.metadata.labelPlural,
        icon: CONFIG.JournalEntry.sidebarIcon
    },
    tables: {
        tooltip: RollTable.metadata.labelPlural,
        icon: CONFIG.RollTable.sidebarIcon
    },
    cards: {
        tooltip: Cards.metadata.labelPlural,
        icon: CONFIG.Cards.sidebarIcon
    },
    playlists: {
        tooltip: Playlist.metadata.labelPlural,
        icon: CONFIG.Playlist.sidebarIcon
    },
    compendium: {
        tooltip: "SIDEBAR.TabCompendium",
        icon: "fas fa-atlas"
    },
    settings: {
        tooltip: "SIDEBAR.TabSettings",
        icon: "fas fa-cogs"
    }
};

export const COMBAT = {
    begin: {
        label: 'COMBAT.Begin',
    },
    prevRound: {
        label: 'COMBAT.RoundPrev',
    },
    prevTurn: {
        label: 'COMBAT.TurnPrev',
    },
    nextTurn: {
        label: 'COMBAT.TurnNext',
    },
    nextRound: {
        label: 'COMBAT.RoundNext',
    },
    rollAll: {
        label: 'COMBAT.RollAll',
    },
    rollNPC: {
        label: 'COMBAT.RollNPC',
    },
    pingCurrent: {
        label: 'LAYER_HOTKEYS.pingCombatant',
        hint: 'Note: Shift or Alt modifiers result in pan or alert pings'
    },
}