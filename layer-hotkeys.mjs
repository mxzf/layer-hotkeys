import {DEFAULT_BINDINGS, CORE_LAYERS, TABS, COMBAT} from "./const.mjs"

function activate(layer) {
    if (ui.controls.control.layer === layer && game.settings.get('layer-hotkeys','cycle')) {
        // Find all the tools which can be selected raher than ones that are just clicked/toggled
        let tools = ui.controls.control.tools.filter(t=> !(t?.button || t?.toggle)).map(t => t.name)
        let next = tools[(tools.findIndex(t=>t===ui.controls.activeTool)+1)%tools.length]
        ui.controls.initialize({tool:next})
    } else {
        if (game.settings.get('layer-hotkeys', 'resetOnChange')) {
            canvas[layer].activate({tool:ui.controls.controls.find(l => l.layer === layer).tools[0].name})
        } else canvas[layer].activate()
    }
}

function activateTool(layer, tool) {
    const tool_data = ui.controls.controls.find(l => l.layer === layer).tools.find(t => t.name === tool);
    if (tool_data?.toggle) { // Handle toggle buttons (like overhead tiles and wall snapping)
        tool_data.active = !tool_data.active;
        tool_data.onClick(tool_data.active)
        if (!canvas[layer].active) canvas[layer].activate(); // Doing the changes in this order lets it render the proper state
        ui.controls.initialize()
    } else if (tool_data?.button) { // Handle click buttons (like the various delete all buttons)
        if (!canvas[layer].active) canvas[layer].activate();
        tool_data.onClick() 
    } else {
        if (canvas[layer].active) ui.controls.initialize({ tool });
        else canvas[layer].activate({ tool });
    }
}

function combatActions(action){
    switch (action) {
        case 'begin': return game.combat.startCombat()
        case 'prevRound': return game.combat.previousRound()
        case 'prevTurn': return game.combat.previousTurn()
        case 'nextTurn': return game.combat.nextTurn()
        case 'nextRound': return game.combat.nextRound()
        case 'rollAll': return game.combat.rollAll()
        case 'rollNPC': return game.combat.rollNPC()
        case 'pingCurrent': return canvas.ping(game.combat.combatant.token.object.center)
        default:
            break;
    }
}

Hooks.on('init', () => {
    game.settings.register('layer-hotkeys', 'cycle', {
      name: "LAYER_HOTKEYS.cycle",
      hint: 'LAYER_HOTKEYS.cycleTip',
      scope: "client",
      config: true,
      requiresReload: false,
      type: Boolean,
      default: false
    });
    game.settings.register('layer-hotkeys', 'resetOnChange', {
      name: "LAYER_HOTKEYS.resetOnChange",
      hint: 'LAYER_HOTKEYS.resetOnChangeTip',
      scope: "client",
      config: true,
      requiresReload: false,
      type: Boolean,
      default: false
    });
    
    game.settings.register('layer-hotkeys', 'layers', {
        scope: 'client',
        config:false,
        type: Array,
        requiresReload: true,
        default: null
    })
    
    game.settings.register('layer-hotkeys','sidebar-hotkeys',{
        name: "LAYER_HOTKEYS.sidebarHotkeys",
        hint: 'LAYER_HOTKEYS.sidebarHotkeysTip',
        scope: 'client',
        config: true,
        type: Boolean,
        requiresReload: true,
        default: false
    })

    game.settings.register('layer-hotkeys','combat-hotkeys',{
        name: "LAYER_HOTKEYS.combatHotkeys",
        hint: 'LAYER_HOTKEYS.combatHotkeysTip',
        scope: 'client',
        config: true,
        type: Boolean,
        requiresReload: true,
        default: false
    })


    // Iterate through all of the core layers, registering settings for registering keybinds for each of them
    for (const layer of game.settings.get('layer-hotkeys', 'layers') ?? CORE_LAYERS) {
        game.settings.register('layer-hotkeys', layer.layer, {
            name: layer.title,
            hint: "LAYER_HOTKEYS.hotkeyGroupHint",
            scope: 'client',
            config: true,
            requiresReload: true,
            type: Boolean,
            default: false
        })
    }
})

Hooks.on('i18nInit', () => {
    // Iterate through all of the layers registering hotkeys
    for (const layer of game.settings.get('layer-hotkeys', 'layers') ?? CORE_LAYERS) {
        let hotkey = {
            name: `${game.i18n.localize('LAYER_HOTKEYS.activate')} ${game.i18n.localize(layer.title)}`,
            onDown: () => {activate(layer.layer)}
        }
        if (DEFAULT_BINDINGS?.[layer.layer]) hotkey.editable = DEFAULT_BINDINGS[layer.layer]
        game.keybindings.register("layer-hotkeys", layer.layer, hotkey)

        // If the appropriate setting is set, loop through the tools of this layer to register hotkeys
        if (game.settings.get('layer-hotkeys', layer.layer)) {
            for (const tool of layer.tools) {
                let hotkey = {
                    name: `⤷ ${game.i18n.localize(layer.title)} - ${game.i18n.localize(tool.title)}`, //⤷⮱↳➥
                    onDown: () => {activateTool(layer.layer, tool.name)}
                }
                game.keybindings.register("layer-hotkeys", `${layer.layer}_${tool.name}`, hotkey)
            }
        }
    }

    if (game.settings.get('layer-hotkeys','sidebar-hotkeys')){
        for (const [tab,data] of Object.entries(TABS)) {
            game.keybindings.register('layer-hotkeys', tab, {
                name: `${game.i18n.localize('LAYER_HOTKEYS.focus')} ${game.i18n.localize(data.tooltip)}`,
                onDown: () => ui.sidebar.activateTab(tab)
            })
        }
    }
    if (game.settings.get('layer-hotkeys','combat-hotkeys')){
        for (const [action,data] of Object.entries(COMBAT)) {
            game.keybindings.register('layer-hotkeys', action, {
                name: `${game.i18n.localize('LAYER_HOTKEYS.combatPrefix')} ${game.i18n.localize(data.label)}`,
                hint: data.hint ?? "",
                onDown: () => combatActions(action)
            })
        }
    }
})

Hooks.on('renderSettingsConfig', (app, el, data) => {
    // Insert preview icon
    let buttons = $(`<div class="form-group">
        <label>Save layer list</label>
        <button type="button" data-key="layer-hotkeys.cache" style="flex:1"><i class="fas fa-floppy-disk"></i><label>${game.i18n.localize('LAYER_HOTKEYS.cache.save')}</label></button>
        <button type="button" data-key="layer-hotkeys.reset" style="flex:1"><i class="fas fa-times"></i><label>${game.i18n.localize('LAYER_HOTKEYS.cache.reset')}</label></button>
        <p class="notes">${game.i18n.localize('LAYER_HOTKEYS.cache.notes')}</p>
    </div>`)
    buttons.find('[data-key="layer-hotkeys.cache"]').click(ev => {
        game.settings.set('layer-hotkeys', 'layers',ui.controls.controls.map(({layer,title,tools}) => ({layer,title,tools:tools.map(({name,title,button,toggle}) => ({name,title,button,toggle}))})))
        ui.notifications.info(game.i18n.localize('LAYER_HOTKEYS.cache.saveSettings'))
    })
    buttons.find('[data-key="layer-hotkeys.reset"]').click(ev => {
        game.settings.set('layer-hotkeys', 'layers',null)
        ui.notifications.info(game.i18n.localize('LAYER_HOTKEYS.cache.resetSettings'))
    })
    el.find('[data-tab="layer-hotkeys"] h2').after(buttons)
});